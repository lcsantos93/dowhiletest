module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        files: {
          "css/user-list.css": "less/user-list.less",
          "css/user.css": "less/user.less"
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'js/test.min.js': [
            'js/bootstrap.min.js'
          ]
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          // sourceMap: 'assets/js/scripts.min.js.map',
          // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
        }
      }
    },
    watch: {
      styles: {
        files: ['**/*.less'],
        tasks: ['less']
      },
      js: {
        files: ['js/*.js'],
        tasks: ['uglify']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: true
        },
        files: [
          'js/bootstrap.min.js'
        ]
      }
    },

  });

  grunt.registerTask('default', ['less', 'watch', 'uglify']);
};