let express = require('express')
let cons = require('consolidate')
let swig = require('swig') 
let app = express()
let path = require('path');
let mangoose = require('mongoose')
let jquery = require('jquery')
let popper = require('popper.js')
let favicon = require('serve-favicon');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

//let bootstrap = require('bootstrap')
let port = 3000
const ip = 'localhost'


let routes = require('./routes/index');
let users = require('./routes/users');

app.engine('html', cons.swig)
app.set('view engine', 'html')
app.set('views',__dirname+'/views');
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/scripts', express.static(__dirname + '/node_modules/jquery/dist/'));    
app.use('/scripts', express.static(__dirname + '/node_modules/popper.js/dist/'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/svg', express.static(__dirname + '/public/svg/'));

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

app.use( (req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(port, ip, () => {
  console.log(`Servidor rodando em http://${ip}:${port}`)
  console.log('Para derrubar o servidor: ctrl + c');
})    


module.exports = app;