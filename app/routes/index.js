let express = require('express'),
    router = express.Router(),
    assert = require('assert') 

    router.get('/', (req, res) => {
  res.render('user')  
})
    
router.get('/user', (req, res) => {
  res.render('user')
})

/* GET Userlist page. */
router.get('/userlist', (req, res) => {
  let db = require("../db");
  let Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
  Users.find({}).lean().exec(  (e, docs) => {
    res.render('userList', { "userList": docs });
    //res.render('user')
  
  });
});


/* POST to Add User Service */
router.post('/adduser', (req, res)  => {
  
  let db = require("../db"),
      userName = req.body.username,
      userEmail = req.body.useremail,
      userCpf = req.body.cpf,
      userTelefone = req.body.telefone,
      userIsActive = req.body.isActive,
      userNascimento = new Date(req.body.nascimento).toISOString().substr(0, 10).toString();
      console.log("this is DATEEEEEE",userNascimento);
  let Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
  let user = new Users({ username: userName, email: userEmail, cpf: userCpf, telefone: userTelefone, isActive: userIsActive, nascimento: userNascimento  });
  user.save( (err) => {
    if (err) {
      console.log("Error! " + err.message);
      return err;
    }
    else {
      console.log("Post saved");
      res.redirect("userlist");
    }
  });
});


module.exports = router;