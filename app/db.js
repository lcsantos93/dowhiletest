let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/crudusersdb');

const userSchema = new mongoose.Schema({
  username: { 
    type: String,
    required: true
  },    
  email: {
    type: String,
    lowercase: true,
    required: false,
    validate: {
      isAsync: true,
      validator: (value, isValid) => {
        const self = this;
        return self.constructor.findOne({ email: value })
        .exec( (err, user) => {
          if(err){
              throw err;
          }
          else if(user) {
              if(self.id === user.id) {
                  return isValid(true);
              }
              return isValid(false);  
          }
          else{
              return isValid(true);
          }

        })
      },
      message:  'The email address is already taken!'
    },
  },
  cpf: { 
    type: Number,
    required: true
  },    
  telefone: Number, 
  isActive: Boolean,
  nascimento: Date
}, { collection: 'usercollection' }
);
 
module.exports = { Mongoose: mongoose, UserSchema: userSchema }